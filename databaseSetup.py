#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sqlite3
from src.constants import DATA_DIR

if __name__ == "__main__":
    conn = sqlite3.connect(os.path.join(DATA_DIR, "users.db"))
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE Users (
        fullname VARCHAR(200),
        email_address VARCHAR(200) PRIMARY KEY,
        password CHAR(20)
    )""")
    cursor.close()
    conn.commit()
    conn.close()
    print("Created database at {DATA_DIR}")
