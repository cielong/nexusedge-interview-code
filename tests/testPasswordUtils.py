#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import env
import string
import unittest
from src.PasswordUtils import PasswordUtils


class testPasswordUtils(unittest.TestCase):
    def testPasswordGenerationWeak(self):
        password = PasswordUtils.generate_password(9, 1)
        self.assertRegex(password, r"[a-z]{9}")

    def testPasswordGenerationMedium(self):
        password = PasswordUtils.generate_password(9, 2)
        self.assertRegex(password, r"[a-z0-9]{9}")
        self.assertRegex(password, r".*[0-9].*")

    def testPasswordGenerationStrong(self):
        password = PasswordUtils.generate_password(9, 3)
        self.assertRegex(password, r"[a-z0-9A-Z]{9}")
        self.assertRegex(password, r".*[0-9].*")
        self.assertRegex(password, r".*[A-Z].*")

    def testPasswordGenerationExtremeStrong(self):
        password = PasswordUtils.generate_password(9, 4)
        pattern = r"[a-z0-9A-Z" + string.punctuation + r"]{9}"
        self.assertRegex(password, pattern)
        self.assertRegex(password, r".*[0-9].*")
        self.assertRegex(password, r".*[A-Z].*")
        self.assertRegex(password, r".*[" + string.punctuation + r"].*")

    def testPasswordValidationWeak(self):
        password = "abcde"
        level = PasswordUtils.check_password_level(password)
        self.assertEqual(level, 1)

    def testPasswordValidationMedium(self):
        password = "1abcde"
        level = PasswordUtils.check_password_level(password)
        self.assertEqual(level, 2)

    def testPasswordValidationStrong(self):
        password = "a1bcFde"
        level = PasswordUtils.check_password_level(password)
        self.assertEqual(level, 3)

    def testPasswordValidationExtremeStrong(self):
        password = "ab1c_de@K@"
        level = PasswordUtils.check_password_level(password)
        self.assertEqual(level, 4)

    def testPasswordValidationMediumLengthOverEight(self):
        password = "abcdefghi"
        level = PasswordUtils.check_password_level(password)
        self.assertEqual(level, 2)

    def testPasswordValidationStrongLengthOverEight(self):
        password = "abcdefghi1"
        level = PasswordUtils.check_password_level(password)
        self.assertEqual(level, 3)
