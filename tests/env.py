#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    This module is intended to register make code under src directory
    accessible by test code
"""

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                '..')))
