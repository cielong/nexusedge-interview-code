#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import random
import string
from textwrap import dedent


class PasswordUtils():
    level2charset = [string.ascii_lowercase, string.digits,
                     string.ascii_uppercase, string.punctuation]
    charset2level = {char: level+1
                     for level, charset in enumerate(level2charset)
                     for char in charset}

    @staticmethod
    def generate_password(length:  int, complexity: int):
        """Generate a random password with given length and complexity

        Parameters:
        -----------
            length: number of characters
            complexity: complexity level, currently range (1, 4)

        Returns:
        --------
            generated password
        """
        random.seed(time.clock())
        if length < complexity:
            err_string = f"""The length of the password should be longer
                             than the complexity level in order to satisfy
                             the complexity requirements, got length
                             {length} < complexity {complexity}"""
            raise ValueError(dedent(err_string))
        elif complexity not in range(1, len(PasswordUtils.level2charset)+1):
            err_string = f"""The complexity level is not correct, expecting the
                             level an integer is within the range of
                             [1, {len(PasswordUtils.level2charset)}],
                             got {complexity}"""
            raise ValueError(dedent(err_string))
        # first for each complexity lower than the level specified
        # select one character
        password = [random.choice(PasswordUtils.level2charset[i-1])
                    for i in range(complexity, 0, -1)]

        # generate the rest characters using the according range
        charset = "".join(PasswordUtils.level2charset[:complexity])
        password.extend([random.choice(charset)
                         for _ in range(length - complexity)])

        # shuffle the list to reduce problem cause by the string
        # generation order
        random.shuffle(password)
        return "".join(password)

    @staticmethod
    def check_password_level(password: str):
        """Return the password complexity level for a given password

        Parameters
        ----------
           password: password

        Returns
        -------
           complexity level
        """
        levels = set([PasswordUtils.charset2level[c] for c in password])
        if levels == {1}:
            return 1 if len(password) < 8 else 2
        elif levels == {1, 2}:
            return 2 if len(password) < 8 else 3
        elif levels == {1, 2, 3}:
            return 3
        elif levels == {1, 2, 3, 4}:
            return 4
        else:
            err_string = f"""Complexity level of password `{password}` cannot
                             be assigned, it fails the password pre-defined
                             complexity level formating."""
            raise ValueError(dedent(err_string))
