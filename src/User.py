#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import requests


# datebase interaction
from src.databaseUtils import executeUpdate


def create_user(db_path):
    """Create a user using "https://randomuser.me/api"

    Parameters
    ----------
        db_path: the database name

    Returns
    -------
        list of users been created, on error return empty list
    """
    response = requests.get("https://randomuser.me/api")
    if (response.status_code == requests.codes.ok):
        # get user info
        user = parse_response(response)
        # persist in database
        executeUpdate(db_path,
                      """INSERT INTO Users (fullname, email_address)
                         VALUES (?, ?)""",
                      user)
        return user
    else:
        print("Retrieving user info fails. It may cause by network error.",
              file=sys.stderr)
        return []


def parse_response(response):
    """Parse response returned from randomuser api

    Parameters
    ----------
        response: the response returned by randomuser api

    Returns
    -------
        list of fullname, email_address extracted from the response
    """
    users = []
    response = response.json()
    if "results" in response:
        results = response["results"]
        for user in results:
            fullname = ""
            email = ""
            # find name
            if "name" in user:
                name = user["name"]
                if "first" in name:
                    fullname += name["first"]
                if "last" in name:
                    fullname += f" {name['last']}"
            # find email address
            if "email" in user:
                email = user["email"]
            users.append((fullname, email))
    return users
