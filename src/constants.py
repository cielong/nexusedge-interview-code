#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os

CUR_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.abspath(os.path.join(CUR_DIR, "..", "data"))
