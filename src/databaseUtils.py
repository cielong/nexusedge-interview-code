#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import sqlite3

from src.constants import DATA_DIR


def executeUpdate(db_path, SQL, values):
    """Execute a SQL command with db_path

    Parameters
    ----------
        db_path: the name of the database
        SQL: the sql command that sqlite3 excuted
        values: list of the values to be inserted/updated

    No returns
    """
    conn = None
    db_path = os.path.join(DATA_DIR, db_path)
    try:
        conn = sqlite3.connect(db_path)
        cursor = conn.cursor()
        cursor.executemany(SQL, values)
        cursor.close()
        conn.commit()
    except Exception as err:
        if conn:
            conn.rollback()
        print(f"{err}", file=sys.stderr)
        raise
    finally:
        if conn:
            conn.close()


def executeQuery(db_path, SQL, values=None):
    """Execute a SQL command with db_path

    Parameters
    ----------
        db_path: the name of the database
        SQL: the sql command (a query) that sqlite3 excuted
        values: values for filtering

    No returns
    """
    conn = None
    db_path = os.path.join(DATA_DIR, db_path)
    try:
        conn = sqlite3.connect(db_path)
        cursor = conn.cursor()
        if values:
            cursor.execute(SQL, values)
        else:
            cursor.execute(SQL)
        result = cursor.fetchall()
        cursor.close()
        conn.commit()
        return result
    except Exception as err:
        if conn:
            conn.rollback()
        print(f"{err}", file=sys.stderr)
        raise
    finally:
        if conn:
            conn.close()
