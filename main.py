#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# password generation
import random
from src.PasswordUtils import PasswordUtils

# user creation
from src.User import create_user

# database interaction
from src.databaseUtils import executeUpdate, executeQuery


if __name__ == "__main__":
    db_path = "users.db"
    for i in range(10):
        user = create_user(db_path)
        if len(user) > 0:
            _, email = user[0]
            length = random.randint(6, 12)
            complexity = random.choice([1, 2, 3, 4])
            password = PasswordUtils.generate_password(length, complexity)
            executeUpdate(db_path, """UPDATE Users SET password = ?
                                      where email_address = ?""",
                          [(password, email)])
        else:
            print(f"Error in creating a user.")
