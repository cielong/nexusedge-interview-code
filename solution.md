# Password Generation and User Creation
This project is mainly to write a simple python project
for nexusedge interview code.

## File Structure
```
.
├── README.md
├── data
│   └── users.db
├── databaseSetup.py
├── main.py
├── reference.py
├── solution.md
├── src
│   ├── PasswordUtils.py
│   ├── User.py
│   ├── __init__.py
│   ├── constants.py
│   └── databaseUtils.py
└── tests
    └── testPasswordUtils.py
```

## SetUp
to start this project, you'll need to first create a database as in **databaseSetup.py**.
After cloning it, run
```bash
	python databaseSetup.py
```
to create the database.

## Start
run
```bash
	python main.py
```
it will create 10 random user in the database. Currently there's no implementation which of them are, it could be implemented using `executeQuery` under
databaseUtils.py file.

## Explanation
The codes are structured as above, it separates each directory with its functionality and in the mean time hide the realization details.

**main.py** is the entry point of the project, which is a demo to how to create the user and persist in users.db under data folder.

### data directory
Database is stored under this folder

### src directory
All the source code is stored under this folder
**PasswordUtils.py** is used for generating password and password validation.
**User.py** is a module contains function to create user. (we can use sqlslchemy or other orm framework here to define User as a class, which will make the codes more clear, but it means more work and may be over engineer here.)
**constants.py** is a module for constants definition, here it only provides the path of the data folder, which allows the database connection.
**databaseUtils.py** is a module for managing database access, like update and query.

### tests directory
All the unittest code is under this folder
**testPasswordUtils.py** is a unittest code for testting whether password generation works correctly.

